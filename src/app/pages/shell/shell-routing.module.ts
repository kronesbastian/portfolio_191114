import { HeaderComponent } from '../../components/header/header.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ShellComponent } from './shell.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    component: ShellComponent,
    children: [
      {
        path: '',
        loadChildren: () =>
          import('../skills/skills.module').then(m => m.SkillsModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class ShellRoutingModule {}
