import { CommonModule } from '@angular/common';
import { HeaderComponent, SidenavComponent } from '@components';
import { MaterialModule } from 'core/theming/material.module';
import { NgModule } from '@angular/core';
import { ShellComponent } from './shell.component';
import { ShellRoutingModule } from './shell-routing.module';
import { SkillsComponent } from '../skills/skills.component';
import { SkillsModule } from '../skills/skills.module';
@NgModule({
  declarations: [
    ShellComponent,
    HeaderComponent,
    SidenavComponent,
    SkillsComponent
  ],
  imports: [CommonModule, ShellRoutingModule, SkillsModule, MaterialModule]
})
export class ShellModule {}
