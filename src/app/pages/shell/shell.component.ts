import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit
  } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { map, shareReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { SetAppIsLoading } from 'core/store/actions';
import { Store } from '@ngxs/store';

@Component({
  selector: 'folio-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ShellComponent implements AfterViewInit {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private store: Store,
    private _cd: ChangeDetectorRef,
    private breakpointObserver: BreakpointObserver
  ) {}

  ngAfterViewInit() {
    this.store.dispatch(new SetAppIsLoading(false));
  }
}
