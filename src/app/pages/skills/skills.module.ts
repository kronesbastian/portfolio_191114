import { CommonModule } from '@angular/common';
import { MaterialModule } from 'core/theming/material.module';
import { NgModule } from '@angular/core';
import { SkillsRoutingModule } from './skills-routing.module';


@NgModule({
  declarations: [],
  imports: [CommonModule, SkillsRoutingModule, MaterialModule]
})
export class SkillsModule {}
