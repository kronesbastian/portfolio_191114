type SkillType = 'Framework' | 'Library' | 'DevOp' | 'IDE' | 'Language';

export enum skillType {
  Framework = 'Framework',
  Library = 'Library',
  DevOp = 'DevOp',
  IDE = 'IDE',
  Language = 'Language'
}

export interface Skill {
  name: string;
  level: number;
  lastUpdate: Date;
  type: SkillType;
}
