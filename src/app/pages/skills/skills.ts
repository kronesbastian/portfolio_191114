import { Skill, skillType } from './skill.model';

export const skills: Skill[] = [
  {
    name: 'Angular',
    level: 80,
    lastUpdate: new Date(2019, 3),
    type: skillType.Framework
  },
  {
    name: 'Typescript',
    level: 80,
    lastUpdate: new Date(2019, 3),
    type: skillType.Language
  },
  {
    name: 'Javascript',
    level: 60,
    lastUpdate: new Date(2019, 3),
    type: skillType.Language
  },
  {
    name: 'HMTL5',
    level: 90,
    lastUpdate: new Date(2019, 3),
    type: skillType.Language
  },
  {
    name: 'CSS3',
    level: 80,
    lastUpdate: new Date(2019, 3),
    type: skillType.Language
  },
  {
    name: 'SCSS',
    level: 80,
    lastUpdate: new Date(2019, 3),
    type: skillType.Language
  },
  {
    name: 'Angular-Material',
    level: 50,
    lastUpdate: new Date(2018, 12),
    type: skillType.Library
  },
  {
    name: 'NGXS',
    level: 70,
    lastUpdate: new Date(2019, 3),
    type: skillType.Library
  },
  {
    name: 'NGRX',
    level: 20,
    lastUpdate: new Date(2018, 6),
    type: skillType.Library
  },
  {
    name: 'RxJS',
    level: 60,
    lastUpdate: new Date(2019, 3),
    type: skillType.Library
  },
  {
    name: 'Python',
    level: 40,
    lastUpdate: new Date(2018, 2),
    type: skillType.Language
  },
  {
    name: 'C#',
    level: 50,
    lastUpdate: new Date(2015, 8),
    type: skillType.Language
  },
  {
    name: 'C',
    level: 30,
    lastUpdate: new Date(2017, 6),
    type: skillType.Language
  },
  {
    name: 'Ionic',
    level: 40,
    lastUpdate: new Date(2017, 7),
    type: skillType.Framework
  },
  {
    name: 'Bootstrap',
    level: 50,
    lastUpdate: new Date(2017, 6),
    type: skillType.Library
  },
  {
    name: 'ASP.NET',
    level: 50,
    lastUpdate: new Date(2015, 8),
    type: skillType.Framework
  },
  {
    name: 'Firebase',
    level: 50,
    lastUpdate: new Date(2019, 3),
    type: skillType.Library
  },
  {
    name: 'Firestore',
    level: 60,
    lastUpdate: new Date(2019, 3),
    type: skillType.Library
  },
  {
    name: 'Angular Flex-Layout',
    level: 30,
    lastUpdate: new Date(2019, 3),
    type: skillType.Library
  },
  {
    name: 'PHP',
    level: 40,
    lastUpdate: new Date(2018, 5),
    type: skillType.Language
  },
  {
    name: 'Visual Studio Code',
    level: 80,
    lastUpdate: new Date(2019, 3),
    type: skillType.IDE
  },
  {
    name: 'Atom',
    level: 10,
    lastUpdate: new Date(2013, 1),
    type: skillType.IDE
  },
  {
    name: 'Aptana',
    level: 10,
    lastUpdate: new Date(2009, 1),
    type: skillType.IDE
  },
  {
    name: 'Riders',
    level: 10,
    lastUpdate: new Date(2019, 5),
    type: skillType.IDE
  },
  {
    name: 'Webstorm',
    level: 10,
    lastUpdate: new Date(2019, 5),
    type: skillType.IDE
  },
  {
    name: 'Visual Studio',
    level: 50,
    lastUpdate: new Date(2015, 8),
    type: skillType.IDE
  },
  {
    name: 'Grunt',
    level: 40,
    lastUpdate: new Date(2015, 8),
    type: skillType.IDE
  },
  {
    name: 'Gulp',
    level: 50,
    lastUpdate: new Date(2015, 8),
    type: skillType.DevOp
  },
  {
    name: 'Gitlab',
    level: 30,
    lastUpdate: new Date(2019, 2),
    type: skillType.DevOp
  },
  {
    name: 'Circle-CI',
    level: 40,
    lastUpdate: new Date(2018, 12),
    type: skillType.DevOp
  },
  {
    name: 'Travis-CI',
    level: 20,
    lastUpdate: new Date(2018, 4),
    type: skillType.DevOp
  },
  {
    name: 'Nx',
    level: 30,
    lastUpdate: new Date(2019, 3),
    type: skillType.DevOp
  }
];
