import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewChild
  } from '@angular/core';
import { map, shareReplay } from 'rxjs/operators';
import { MatSidenav } from '@angular/material';
import { Observable } from 'rxjs';

@Component({
  selector: 'folio-sidenav',
  templateUrl: './sidenav.component.html',
  styleUrls: ['./sidenav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidenavComponent {
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  @ViewChild('drawer', { static: true }) drawer: any;

  constructor(private breakpointObserver: BreakpointObserver) {}

  ngAfterViewInit() {
    console.warn(this.drawer);
  }
}
