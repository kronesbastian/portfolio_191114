import {
  Action,
  Selector,
  State,
  StateContext
  } from '@ngxs/store';
import { CoreStateModel } from './core.model';
import { SetAppIsLoading } from './actions';

@State<CoreStateModel>({
  name: 'PORTFOLIO',
  defaults: {
    appIsLoading: true
  }
})
export class CoreState {
  @Selector()
  static appIsLoading(state: CoreStateModel) {
    return state.appIsLoading;
  }

  @Action(SetAppIsLoading)
  setAppIsLoading(
    { getState, patchState }: StateContext<CoreStateModel>,
    { appIsLoading }: SetAppIsLoading
  ) {
    const state = getState();

    patchState({
      ...state,
      appIsLoading
    });
  }
}
