export class SetAppIsLoading {
  static readonly type = '[Core] SetAppIsLoading';
  constructor(public appIsLoading: boolean) {}
}
