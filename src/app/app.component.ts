import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit
  } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { CoreState } from './core/store/core.state';
import { first, map, shareReplay } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { Select, Store } from '@ngxs/store';
import { SetAppIsLoading } from './core/store/actions';
@Component({
  selector: 'folio-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit, AfterViewInit {
  @Select(CoreState.appIsLoading) appIsLoading$: Observable<boolean>;

  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches),
      shareReplay()
    );

  constructor(
    private store: Store,
    private _cd: ChangeDetectorRef,
    private breakpointObserver: BreakpointObserver
  ) {}

  ngOnInit() {
    this.store.dispatch(new SetAppIsLoading(true));
  }

  ngAfterViewInit() {
    /**
     * Check view if deepest component is loaded
     */
    this.appIsLoading$
      .pipe(first(isLoading => !isLoading))
      .subscribe(_ => this._cd.detectChanges());
  }
}
